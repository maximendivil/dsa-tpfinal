class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :name, null: false
      t.string :organization, null: false
      t.boolean :is_approved, default: false
      t.string :token

      t.timestamps
    end
  end
end
