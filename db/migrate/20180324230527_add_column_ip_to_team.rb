class AddColumnIpToTeam < ActiveRecord::Migration[5.1]
  def change
    add_column :teams, :ip, :string
  end
end
