class CreatePageConfigurations < ActiveRecord::Migration[5.1]
  def change
    create_table :page_configurations do |t|
      t.string :value, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
