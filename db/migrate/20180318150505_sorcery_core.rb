class SorceryCore < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :role, null: false, default: 1
      t.string :email, null: false
      t.string :crypted_password
      t.string :salt
      t.belongs_to :team
      t.boolean :is_captain, default: false

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
