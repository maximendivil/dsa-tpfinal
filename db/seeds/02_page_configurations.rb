# frozen_string_literal: true
puts 'BEGIN Seed Page Configurations'
puts '----------------'

configuration = PageConfiguration.where(id: 1).first_or_initialize do |config|
  config.name = 'Jugadores por equipo'
  config.value = '5'
end

if configuration.new_record?
  configuration.save!
  puts "PageConfiguration created => #{configuration.name}, #{configuration.value}"
end

puts "PageConfiguration count: #{PageConfiguration.count}"

puts '----------------'
puts 'END Seed Page Configurations'
