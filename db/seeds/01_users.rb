# frozen_string_literal: true
puts 'BEGIN Seed Users'
puts '----------------'

admin = User.where(email: 'trabajofinaldsa@gmail.com').first_or_initialize do |user|
  user.username              = 'admin'
  user.first_name            = 'Admin'
  user.last_name             = 'Admin'
  user.role                  = 'admin'
  user.password              = '123123123'
  user.password_confirmation = '123123123'
end

if admin.new_record?
  admin.save!
  puts "User created => #{admin.first_name}, #{admin.last_name}, #{admin.email}, "\
          '12313213'
end

puts "User count: #{User.count}"

puts '----------------'
puts 'END Seed Users'
