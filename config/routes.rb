Rails.application.routes.draw do
  get 'password_resets/create'

  get 'password_resets/edit'

  get 'password_resets/update'

  resources :page_configurations, only: [:index, :edit, :update]

  resources :password_resets, only: [:new, :create, :edit, :update]

  resources :teams, only: [:index, :new, :create, :edit, :update, :destroy] do
    put :confirm, on: :member
    collection do
      get :recruit
      post :recruit_user
    end
  end
  resources :user_sessions, only: [:new, :create, :destroy]
  resources :users, only: [:index, :new, :create, :edit, :update, :destroy] do
    delete :delete_team, on: :member
  end

  get 'login' => 'user_sessions#new', :as => :login
  post 'logout' => 'user_sessions#destroy', :as => :logout
  root to: 'main#home'

  get 'panel_one'   => 'main#panel_one'
  get 'panel_two'   => 'main#panel_two'
  get 'panel_three' => 'main#panel_three'
  get 'panel_four'  => 'main#panel_four'
  get 'panel_five'  => 'main#panel_five'
  get 'panel_six'   => 'main#panel_six'

  namespace :api do
    get 'get_teams' => 'api#get_teams'
    get 'get_teams_and_users' => 'api#get_teams_and_users'
    post 'nuevo_juego' => 'api#new_play'
  end
end
