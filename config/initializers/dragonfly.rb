require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "57da854e55809da7ef9ff63ac07f903482b21b3cb0f622a8039dc4931ec09c86"

  url_format "/media/:job/:name"

  datastore :file,
    root_path: Rails.root.join('public/system/dragonfly', Rails.env),
    server_root: Rails.root.join('public')
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
ActiveSupport.on_load(:active_record) do
  extend Dragonfly::Model
  extend Dragonfly::Model::Validations
end
