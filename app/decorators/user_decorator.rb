class UserDecorator < Draper::Decorator
  delegate_all

  def name
    "#{user.first_name} #{user.last_name}".titleize
  end

  private
  
  def user
    object
  end
end
