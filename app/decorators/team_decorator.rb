class TeamDecorator < Draper::Decorator
  delegate_all
  decorates_association :users

  def name
    team.name.titleize
  end

  private

  def team
    object
  end
end
