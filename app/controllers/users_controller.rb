class UsersController < ApplicationController
  skip_before_action :require_login, only: [:create]
  before_action :set_user, only: [:show, :edit, :update, :destroy, :delete_team]

  def index
    authorize User
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit
    authorize @user
  end

  def create
    @user = User.new(user_params)
    @user.role = User.roles[:normal]
    authorize @user
    if @user.save
      RecruitedUserToTeamMailer.recruited_user_to_team_email(@user).deliver_now
      redirect_to login_path, notice: 'El usuario se creó exitosamente.'
    else
      redirect_to recruit_user_teams_path
    end
  end

  def update
    authorize @user
    if @user.update(user_params)
      redirect_to home_path, notice: 'El usuario de modificó exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al modificar el usuario. Por favor intente nuevamente'
      render :edit
    end
  end

  def destroy
    authorize @user
    if @user.destroy
      redirect_to users_path, notice: 'El usuario se eliminó exitosamente'
    else
      flash[:error] = 'Ocurrió un error al eliminar el usuario. Por favor intente nuevamente'
      redirect_to users_path
    end
  end

  def delete_team
    team = @user.team
    @user.team = nil
    authorize @user
    if @user.save
      RemoveUserFromTeamMailer.remove_user_from_team_email(@user).deliver_now
      redirect_to edit_team_path(team), notice: 'El usuario se eliminó del equipo exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al eliminar el usuario del equipo. Por favor intente nuevamente'
      redirect_to edit_team_path(team)
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:username, :first_name, :last_name, :email, :password, :password_confirmation, :team_id)
  end
end
