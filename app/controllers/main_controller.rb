class MainController < ApplicationController
  before_action :set_records


  def home; end

  def panel_one
    @record = Struct.new(:name, :offensive, :defensive, :disclosure)
    results = Connection.new.score_of_all_teams.body.deep_symbolize_keys
    results[:puntajes].each do |result|
      if Team.exists?(result[:id_team])
        @records << @record.new(
            Team.find(result[:id_team]).name,
            result[:ofensivo],
            result[:defensivo],
            result[:disclosure]
          )
      end
    end
  end

  def panel_two
    @record = Struct.new(:name, :port, :state)
    results = Connection.new.check_services_team(1).body.deep_symbolize_keys
    results[:servicios].each do |result|
      @records << @record.new(
        result[:nombre],
        result[:puerto]
      ) if result[:estado] == 1
    end
  end

  def panel_three
    @record = Struct.new(:team, :ip, :service, :port)
    results = Connection.new.check_all_services.body.deep_symbolize_keys
    results[:equipos].each do |equipo|
      if Team.exists?(equipo[:id_team])
        equipo[:servicios].each do |service|
          @records << @record.new(
            Team.find(equipo[:id_team]).name,
            equipo[:vm],
            service[:nombre],
            service[:puerto]
          ) if service[:estado] == 1
        end
      end
    end
  end

  def panel_four
    @record = Struct.new(:service, :flags, :up, :total, :disclosure)
    results = Connection.new.count_by_service.body.deep_symbolize_keys
    results[:servicios].each do |service|
      @records << @record.new(
        service[:id_servicio],
        service[:flags_robados],
        service[:up],
        service[:total],
        service[:disclosure]
      )
    end
  end

  def panel_five
    @record = Struct.new(:user, :defensive, :offensive, :disclosure)
    results = Connection.new.team_score.body.deep_symbolize_keys
    results[:puntajes].each do |usuario|
      @records << @record.new(
        User.find(usuario[:id_usuario]).username,
        usuario[:defensivo],
        usuario[:ofensivo],
        usuario[:disclosure]
      )
    end
  end

  def panel_six
    @record = Struct.new(:challenge, :title, :category, :points, :description, :adjunto)
    results = Connection.new.challenges(1).body.deep_symbolize_keys
    results[:desafios].each do |desafio|
      @records << @record.new(
        desafio[:id_desafio],
        desafio[:titulo],
        desafio[:categoria],
        desafio[:puntos],
        desafio[:descripcion],
        desafio[:adjunto]
      )
    end
  end

  private

  def set_records
    @records = []
  end

end
