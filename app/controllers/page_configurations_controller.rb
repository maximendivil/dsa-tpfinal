class PageConfigurationsController < ApplicationController
  before_action :set_page_configuration, only: [:edit, :update]

  def index
    @configurations = PageConfiguration.all
  end

  def edit; end

  def update
    if @page_configuration.update(page_configuration_params)
      redirect_to page_configurations_path, notice: 'La configuración se modificó exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al modificar la configuración. Por favor intente nuevamente'
      render :edit
    end
  end

  private

  def set_page_configuration
    @page_configuration = PageConfiguration.find(params[:id])
  end

  def page_configuration_params
    params.require(:page_configuration).permit(:name, :value)
  end
end
