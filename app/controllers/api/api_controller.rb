class Api::ApiController < ActionController::API

  def get_teams
    teams = Team.all.map do |team|
      {
        "id_team": "#{team.id}",
        "nombre_team": "#{team.name}",
        "vm": team.ip
      }
    end
    render json: { "equipos": teams }, status: :ok
  end

  def get_teams_and_users
    teams = Team.includes(:users).all.map do |team|
      {
        "id_team": "#{team.id}",
        "nombre_team": "#{team.name}",
        "vm": team.ip,
        "usuarios": team.users.map do |user|
          {
            "nombreDeUsuario": user.username, "id_usuario": "#{user.id}"
          }
        end
      }
    end
    render json: { "equipos": teams }, status: :ok
  end

  def new_play
    render json: params[:id_juego]
  end
end
