class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :require_login
  helper_method :home_path

  NOT_AUTHORIZED = 'No tenés los permisos necesarios para realizar la acción '\
    'solicitada'.freeze

  def user_not_authorized
    respond_to do |format|
      format.html do
        flash[:error] = NOT_AUTHORIZED
        redirect_to root_path
      end
      format.js do
        render json: { msg: NOT_AUTHORIZED }, status: :forbidden
      end
    end
  end

  def not_authenticated
    redirect_to login_path, alert: 'Por favor, inicia sesión.'
  end

  def home_path
    return users_path if logged_in? && current_user.admin?
    root_path
  end
end
