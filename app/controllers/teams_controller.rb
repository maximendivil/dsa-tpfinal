class TeamsController < ApplicationController
  skip_before_action :require_login, only: [:new, :create, :recruit, :recruit_user]
  before_action :set_team, only: [:show, :edit, :update, :destroy, :confirm]
  layout 'application_sign_up', only: [:new, :recruit, :recruit_user]

  def index
    authorize Team
    @teams = Team.all.page(params[:page])
  end

  def new
    @team = Team.new
  end

  def edit
    authorize @team
    @team = @team.decorate
  end

  def create
    @team = Team.new(team_params)
    if @team.save
      redirect_to login_path, notice: 'El equipo se creó exitosamente. Deberá esperar la confirmación del administrador para poder ingresar.'
    else
      render :new
    end
  end

  def update
    authorize @team
    if @team.update(team_params)
      redirect_to home_path, notice: 'El equipo se modificó exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al intentar modificar el equipo. Por favor intente nuevamente'
      render :edit
    end
  end

  def destroy
    authorize @team
    if @team.destroy
      redirect_to teams_path, notice: 'El equipo se eliminó exitosamente.'
    else
      flash[:error] = "Ocurrió un error al intentar eliminar un equipo. Por favor intente nuevamente"
      redirect_to teams_path
    end
  end

  def confirm
    authorize @team
    @team.is_approved = true
    @team.token = Digest::MD5.hexdigest(@team.captain.email + Time.now.strftime("%Y/%m/%d %H:%M:%S"))
    if @team.save
      TeamApprovedMailer.team_approved_email(@team).deliver_now
      redirect_to teams_path, notice: 'El equipo se aprobó exitosamente.'
    else
      flash[:error] = 'Ocurrió un error al aprobar el equipo. Por favor intente nuevamente'
      redirect_to teams_path
    end
  end

  def recruit; end

  def recruit_user
    @team = Team.find_by(token: params[:team][:token])
    if @team.present? && @team.has_capacity?
      render :recruit_user
    else
      flash[:error] = 'El token ingresado es inválido o ya se cubrió el cupo de integrantes'
      redirect_to recruit_teams_path
    end
  end

  private

  def set_team
    @team = Team.find(params[:id])
  end

  def team_params
    params.require(:team).permit(:name, :organization, :image, users_attributes: [:username, :first_name, :last_name, :email, :password, :password_confirmation, :is_captain])
  end
end
