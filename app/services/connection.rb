require 'webmock/minitest'

class Connection

  def initialize(url = 'http://localhost:3000')
    @connection = Faraday.new(url: url) do |faraday|
      faraday.request  :url_encoded
      faraday.response :json, content_type: 'application/json'
      faraday.adapter  Faraday.default_adapter

      faraday.options.timeout      = 60_001
      faraday.options.open_timeout = 60_001
    end
    @url = url
    @uri = '/api/'
  end

  def score_of_all_teams
    action = 'score_of_all_teams'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def team_score
    action = 'team_score'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def count_by_service
    action = 'count_by_service'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def challenges(user_id)
    action = 'challenges'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def challenge(user_id, challenge_id)
    action = 'challenge_by_user'
    #este llamado api puede devolver una cosa u otra
    #dependiendo de si el desafio esta disponible para este usuario
    action <<
      if true
        '_ok'
      else
        '_not_ok'
      end

    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def is_solution(problem_id, solution)
    action = 'is_solution'
    #lo mismo que el caso anterior
    action <<
      if true
        '_ok'
      else
        '_not_ok'
      end

    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def get_hint(user_id, hint_id)
    action = 'get_hint'
    #lo mismo que el caso anterior
    action <<
      if true
        '_ok'
      else
        '_not_ok'
      end

    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def check_services_team(team_id)
    action = 'check_services_team'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  def check_all_services
    action = 'check_all_services'
    stub_request :get, action
    @connection.get "#{@uri}#{action}"
  end

  private

  def stub_request(method, action)
    WebMock
      .stub_request(method, /.*#{@uri}#{action}\z/)
      .to_return(
        status:  200,
        body:    File.new("app/mocks/#{action}.json").read,
        headers: {'Content-Type' => 'application/json'}
      )
  end
end
