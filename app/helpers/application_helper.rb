# application_helper.rb
# frozen_string_literal: true
module ApplicationHelper
  def flash_message(type, message)
    toast_type = ''
    toast_type += 'success' if ['success', 'notice'].include? type
    toast_type += 'error'  if ['error', 'danger'].include? type
    toast_type += 'warning' if type == 'warning'
    flash.discard(type)
    toast = "<script>App.showToastr('#{toast_type}', '#{message}');</script>"
    toast.html_safe
  end

  def full_title(page_title)
    base_title = I18n.t :app_name
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def flash_messages
    messages = []
    flash.each do |type, message|
      messages << flash_message(type, message) if message
    end
    messages.join('/n').html_safe
  end

  def close_button(target = :modal)
    link_to '&times;'.html_safe, '', class: :close, data: { dismiss: target }
  end

  def icon(name, html_options = {})
    html_options[:class] = ['fa', "fa-#{name}", html_options[:class]].compact
    content_tag(:i, nil, html_options)
  end

  # Link del menú lateral
  def side_link(text: '', path: '', icon_class: '', klass: '',
                html_options: {}, extra: '')
    link_class = html_options[:class]
    html_options[:class] = link_class
    klass += current_page?(path) ? 'active' : url_for(path)
    content_tag(:li, class: klass) do
      (link_to path, html_options do
        "<i class='fa fa-fw fa-#{icon_class}'></i> <span> #{text} </span>".html_safe
      end) +
        extra.html_safe
    end
  end

  def side_link_status(controller_names_array, class_name = nil)
    if controller_names_array.include?(params[:controller])
      class_name || 'active'
    else
      nil
    end
  end

  def sidebar_state
    'mini-navbar' if content_for :hide_sidebar
  end

  def sub_menu(klass = '', &block)
    klass += ' ' if klass.present?
    klass += 'treeview-menu'
    tree = ''

    if block_given?
      tree = content_tag(:ul, class: klass) do
        capture(&block)
      end
    end

    tree
  end

  def back_to_link(link_text, link_path = '', klass = '', html_options = {})
    link_path = 'javascript:history.back()' if link_path.blank?
    content_tag(:div, class: 'return-back ' + klass) do
      link_to(link_path, options: {}, html_options: html_options) do
        icon('angle-left').html_safe +
          "<span> #{link_text} </span>".html_safe
      end
    end
  end
end
