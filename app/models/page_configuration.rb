class PageConfiguration < ApplicationRecord

  # -- Validations
  validates :name, presence: true
  validates :value, presence: true
end
