class Team < ApplicationRecord

  dragonfly_accessor :image

  # -- Callbacks
  after_create :send_new_team_email

  # -- Validations
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :organization, presence: true
  validates :token, uniqueness: true

  # -- Associations
  has_many :users, dependent: :destroy
  accepts_nested_attributes_for :users

  # -- Methods
  def send_new_team_email
    NewTeamMailer.new_team_email.deliver_now
  end

  def captain
    self.users.find_by(is_captain: true).decorate
  end

  def has_capacity?
    max_capacity = Integer(PageConfiguration.first.value)
    self.users.count < max_capacity
  end
end
