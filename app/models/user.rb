class User < ApplicationRecord
  authenticates_with_sorcery!

  # -- Validations
  validates :password, length: { minimum: 3 }, if: -> { new_record? || changes[:crypted_password] }
  validates :password, confirmation: true, if: -> { new_record? || changes[:crypted_password] }
  validates :password_confirmation, presence: true, if: -> { new_record? || changes[:crypted_password] }
  validates :username, presence: true
  validates :username, uniqueness: true
  validates :email, presence: true
  validates :email, uniqueness: true

  # -- Associations
  belongs_to :team, optional: true

  # -- Enum
  enum role: %i[admin normal]

  # -- Methods
  def deliver_set_password_instructions!
		config = sorcery_config
		return false if config.reset_password_time_between_emails.present? && self.send(config.reset_password_email_sent_at_attribute_name) && self.send(config.reset_password_email_sent_at_attribute_name) > config.reset_password_time_between_emails.seconds.ago.utc
		self.class.sorcery_adapter.transaction do
			generate_reset_password_token!
			send_set_password_email!
		end
	end

	def	send_set_password_email!
		self.class.sorcery_adapter.transaction do
			generate_reset_password_token!
      UserMailer.reset_password_email(self).deliver_now
		end
	end
end
