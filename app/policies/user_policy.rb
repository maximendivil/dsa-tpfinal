# frozen_string_literal: true
class UserPolicy < ApplicationPolicy

  def index?
    user.admin?
  end

  def edit?
    update?
  end

  def update?
    user == record || user.admin?
  end

  def destroy?
    user.admin? && user != record
  end

  def delete_team?
    user.is_captain?
  end
end
