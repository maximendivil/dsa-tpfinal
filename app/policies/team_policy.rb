# frozen_string_literal: true
class TeamPolicy < ApplicationPolicy

  attr_reader :user, :team

  def initialize(user, team)
    @user = user
    @team = team
  end

  def index?
    user.admin?
  end

  def edit?
    update?
  end

  def update?
    user.is_captain?
  end

  def destroy?
    index?
  end

  def confirm?
    index?
  end
end
