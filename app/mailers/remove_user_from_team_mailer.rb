class RemoveUserFromTeamMailer < ApplicationMailer
  def remove_user_from_team_email(user)
    @user = User.find(user.id).decorate
    mail(to: @user.email, subject: "Te eliminaron del equipo - CTF")
  end
end
