class NewTeamMailer < ApplicationMailer
  def new_team_email
    @user = User.find(1).decorate
    mail(to: @user.email, subject: "Solicitud de aprobación de nuevo equipo - CTF")
  end
end
