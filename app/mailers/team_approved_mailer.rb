class TeamApprovedMailer < ApplicationMailer
  def team_approved_email(team)
    @team = Team.find(team.id).decorate
    mail(to: @team.captain.email, subject: "Su equipo fue aprobado - CTF")
  end
end
