class RecruitedUserToTeamMailer < ApplicationMailer
  def recruited_user_to_team_email(user)
    @user = User.find(user.id).decorate
    @captain = User.find(@user.team.captain.id).decorate
    mail(to: @captain.email, subject: "Se enroló un usuario al equipo - CTF")
  end
end
