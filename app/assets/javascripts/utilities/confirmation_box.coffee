App.ConfirmationBox =

  defaults:
    title: 'Advertencia'
    message: '¿Estás seguro?'
    cancel_btn: 'No, cancelar'
    confirm_btn: 'Confirmar'
    confirm_btn_class: 'btn-danger'

  confirmDialog: (element, callback) ->
    title       = element.data('title') || @defaults['title']
    message     = element.data('confirm') || @defaults['message']
    cancel_btn  = element.data('cancel-btn') || @defaults['cancel_btn']
    confirm_btn = element.data('confirm-btn') || @defaults['confirm_btn']
    confirm_btn_class = element.data('confirm-btn-class') || @defaults['confirm_btn_class']
    element.data('confirm', null)

    bootbox.confirm
      title: title
      message: message
      buttons:
        cancel:
          label: cancel_btn
          className: 'btn-default'
        confirm:
          label: confirm_btn
          className: confirm_btn_class
      callback: (result) ->
        if callback
          result && callback()
          return
        else
          element.trigger 'click.rails' if result
          element.data('confirm', message)
          return

$(document).on 'confirm', (event) ->
  element = $(event.target)
  App.ConfirmationBox.confirmDialog(element)
  false
